;;; ElementaryX: Elementary Emacs configuration coupled with Guix
;;;
;;; Extra config: Base enhancements

;;; Usage: Append or require this file from init.el to enable various UI/UX
;;; enhancements.

;;; Contents:
;;;
;;;  - Motion aids
;;;  - Power-ups: Embark and Consult
;;;  - Minibuffer and completion
;;;  - Misc. editing enhancements

(use-package elementaryx-minimal)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Motion aids
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fast navigation with Avy ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://karthinks.com/software/avy-can-do-anything/
(use-package avy
  :demand t
  :bind (("M-g M-l" . avy-goto-line)
	 ("M-g M-g" . avy-goto-char-timer)
	 ;; ("C-c j" . avy-goto-char-timer)
         ("s-j"   . avy-goto-char-timer)))

;;;;;;;;;;;;;;;;;;;;;;
;; Multiple Cursors ;;
;;;;;;;;;;;;;;;;;;;;;;

;; The following use-package should not be necessary
;; However, without it, multiple-cursors does not load properly
;; TODO: investigate why
(use-package multiple-cursors)

;; Note that `C-j' can be used to insert a newline.
(use-package multiple-cursors
  :init
  ;; Add multiple-curosrs "C-c m" leader key
  (which-key-add-key-based-replacements "C-c m" "multiple-cursors")

  ;; Add Selction Menu to the menu-bar. The suggested keys are binding
  ;; from `escode'. They will be suggested only if the `escode' layer
  ;; is enabled. Otherwise the keys defined in the present `base'
  ;; layer (defined below in the `:bind' section) shall be displayed.
  ;; This is because we use `:key-sequence' (rather than `:keys')
  (easy-menu-define elementaryx-selection-map nil
    "Menu for or no selection and multiple-cursors commands."
    '("Selection"
      ["Select All" mark-whole-buffer :help "Select the whole buffer"]
      ["Expand Selection" er/expand-region :help "Select the whole buffer"]
      ["Shrink Selection" er/contract-region :help "Select the whole buffer"]
      "----"
      ["Copy Line Up" elementaryx-copy-region-up :help "Copy Line or Selection Up. Note that you can prefix the command with `C-u N' to duplicate N times."]
      ["Copy Line Down" elementaryx-copy-region-down :help "Copy Line or Selection Down. Note that you can prefix the command with `C-u N' to duplicate N times."]
      ["Move Line Up" move-text-up :help "Move Line or Region Up. Note that you can prefix the command with `C-u N' to move by N lines."]
      ["Move Line Down" move-text-down :help "Move Line or Region Down. Note that you can prefix the command with `C-u N' to move by N lines."]
      "----"
      ;; ["Add Cursor Above"
      ;;  mc/mark-previous-like-this
      ;;  :help "Add Cursor Above (if no selection, otherwise fallback to Add Previous Occurrence)"
      ;;  :keys (lambda ()
      ;;          (let ((binding (where-is-internal 'mc/mark-previous-like-this overriding-local-map t)))
      ;; 		 (if (member [M-S-up] binding)
      ;; 		     "M-S-<up>"
      ;; 		   (key-description (car binding)))))]
      ["Add Cursor Above" mc/mark-previous-like-this :help "Add Cursor Above (if no selection, otherwise fallback to Add Previous Occurrence of or no selection)" :key-sequence "M-S-<up>"]
      ["Add Cursor Below" mc/mark-next-like-this :help "Add Cursor Below (if no selection, otherwise fallback to Add Next Occurrence of or no selection)" :key-sequence "M-S-<down>"]
      ["Add Cursors to Line Ends" mc/edit-ends-of-line "help"]
      ["Add Next Occurrence" mc/mark-next-like-this :help "Add Next Occurrence (if or no selection, otherwise fallback to Add Cursor Below)" :key-sequence "C-d"]
      ["Add Previous Occurrence" mc/mark-previous-like-this :help "Add Next Occurrence (if or no selection, otherwise fallback to Add Cursor Below)" :key-sequence "C-S-d"]
      ["Select All Occurrences" mc/mark-all-dwim :help "Select All Occurrences (if small or no selection, otherwise ask for a pattern to match)" :key-sequence "C-S-l"]
      ["Add Cursor on Click" mc/add-cursor-on-click :help "Use Ctrl+Shift+Click to Add Cursor on Click" :key-sequence "C-S-<mouse-1>"]
      "----"
      ["Do What I Mean" embark-dwim :help "Run the default action on the current target (embark-dwim)"]
      ["Export" embark-export :help "Create a type-specific buffer to manage current candidates (embark-export)"]
      ["Prompt Actions" embark-act :help "Prompt the user for an action and perform it (embark-act)"]
      ))

  ;; Add the menu to the menu bar
  (define-key-after
    (lookup-key global-map [menu-bar])
    [elementaryx-selection-map]
    (cons "Selection" elementaryx-selection-map)
    'edit)

  :config
  ;; DEBUG: uncommment the following line
  ; (message "hello from multiple-cursors")
  :bind
  ;; keyboard bindings
  (
   ;; We will use these bindings in ES Code
   ;; ("C-d" . mark-next-like-this)
   ;; ("C-M-<up>" . mark-previous-like-this)
   ;; ("C-M-<down>" . mark-previous-like-this)
   ("C-c m e" . mc/edit-lines)
   ("C-c m C-a" . mc/edit-beginnings-of-lines)
   ("C-c m C-e" . mc/edit-ends-of-lines)
   ("C-c m p" . mc/mark-previous-like-this)
   ("C-c m n" . mc/mark-next-like-this)
   ("C-c m m" . mc/mark-more-like-this-extended)
   ("C-c m a" . mc/mark-all-dwim)
   ;; ("C-c m a" . mc/mark-all-like-this)
   ("C-c m M" . mc/mark-all-in-region)
   ("C-c m 0" . mc/insert-numbers)
   ("C-c m C-0" . mc/insert-letters)
   ;; mouse bindings
   ("C-S-<mouse-1>" . mc/add-cursor-on-click)))

;; TODO: Set preferences for running commands with multiple cursors.

;; (setq mc/cmds-to-run-for-all
;;       '(
;; 	))

;; (setq mc/cmds-to-run-once
;;       '(
;; 	elementaryx-keyboard-quit
;; 	pixel-scroll-precision
;; 	))




;; ;; Example from Castlemacs (Mac-OS like)
;; ;; Multiple cursors. Similar to Sublime or VS Code.
;; (use-package multiple-cursors
;;   :config
;;   (setq mc/always-run-for-all 1)
;;   (global-set-key (kbd "s-d") 'mc/mark-next-like-this)        ;; Cmd+d select next occurrence of region
;;   (global-set-key (kbd "s-D") 'mc/mark-all-dwim)              ;; Cmd+Shift+d select all occurrences
;;   (global-set-key (kbd "M-s-d") 'mc/edit-beginnings-of-lines) ;; Alt+Cmd+d add cursor to each line in region
;;   (define-key mc/keymap (kbd "<return>") nil))

;;;;;;;;;;;;;;;
;; Move text ;;
;;;;;;;;;;;;;;;
(use-package move-text
  :config
  ;; We bind them together with multiple cursor under `C-c m' leader key
  (global-set-key (kbd "C-c m M-<up>") 'move-text-up)
  (global-set-key (kbd "C-c m M-<down>") 'move-text-down)
  ;; We also bind to M-up and M-down VS Code-like binding
  (move-text-default-bindings)) ;; M-up and M-down for moving text

;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copy line or region ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package emacs
  :config
  ;; We exploit duplate-line and duplicate-region functions introduced
  ;; in emacs 29.1 We define a function with local variable
  ;; `duplicate-line-final-position', which is used to control how
  ;; duplicate-line.
  (defun elementaryx-duplicate-line-with-local-final-position (value)
    "Set `duplicate-line-final-position` locally to VALUE and call `duplicate-line`."
    (interactive "P")  ; Use prefix argument if needed
    (let ((duplicate-line-final-position value))
      (call-interactively #'duplicate-line)))
  ;; We define functions for Copying Line Up and Down
  (defun elementaryx-copy-line-up ()
    "Duplicate the current line and move the copy up."
    (interactive)
    (let ((duplicate-line-final-position 0))
      (call-interactively #'duplicate-line)))
  (defun elementaryx-copy-line-down ()
    "Duplicate the current line and move the copy down."
    (interactive)
    (let ((duplicate-line-final-position -1))
      (call-interactively #'duplicate-line)))
  ;; We define functions for Copying Region Up and Down
  (defun elementaryx-copy-region-up ()
    "Duplicate the current line and move the copy up."
    (interactive)
    (let ((duplicate-line-final-position 0))
      (call-interactively #'duplicate-dwim)))
  (defun elementaryx-copy-region-down ()
    "Duplicate the current line and move the copy down."
    (interactive)
    (let ((duplicate-line-final-position -1))
      (call-interactively #'duplicate-dwim)))
  ;; We bind them together with multiple cursor under `C-c m' leader key
  (global-set-key (kbd "C-c m C-<up>") 'elementaryx-copy-line-up)
  (global-set-key (kbd "C-c m C-<down>") 'elementaryx-copy-line-down)
  (global-set-key (kbd "C-c m <up>") 'elementaryx-copy-region-up)
  (global-set-key (kbd "C-c m <down>") 'elementaryx-copy-region-down)
  ;; We also bind them under `M-C-S' as in VS Code
  ;; (global-set-key (kbd "M-C-S-<up>") 'elementaryx-copy-line-up)
  ;; (global-set-key (kbd "M-C-S-<down>") 'elementaryx-copy-line-down)
  (global-set-key (kbd "M-C-S-<up>") 'elementaryx-copy-region-up)
  (global-set-key (kbd "M-C-S-<down>") 'elementaryx-copy-region-down))
  ;; With lambda:
  ;; Copy Line Up
  ;; (global-set-key (kbd "M-S-<up>") (lambda () (interactive) (elementaryx-duplicate-line-with-local-final-position 0)))
  ;; ;; Copy Line Down
  ;; (global-set-key (kbd "M-S-<down>") (lambda () (interactive) (elementaryx-duplicate-line-with-local-final-position -1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Expand / Shrink Region ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package expand-region
  :bind (("C-c m <right>" . er/expand-region)
         ("C-c m <left>" . er/contract-region)))

(use-package expand-region
  :after elementaryx-escode
  :bind (("M-S-<right>" . er/expand-region)
         ("M-S-<left>" . er/contract-region)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Power-ups: Embark and Consult
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: emacs-consult-eglot, emacs-consult-org-roam, emacs-consult-yasnippet

;; ;; Consult: Misc. enhanced commands
;; (use-package consult
;;   ;; Other good things to bind: consult-line-multi, consult-history,
;;   ;; consult-outline, consult-org-agenda, etc.
;;   :bind (("C-x b" . consult-buffer)  ; orig. switch-to-buffer
;;          ("M-y" . consult-yank-pop)  ; orig. yank-pop
;;          ("M-s r" . consult-ripgrep)
;;          ("C-s" . consult-line))     ; orig. isearch
;;   :config
;;   ;; Narrowing lets you restrict results to certain groups of candidates
;;   (setq consult-narrow-key "<"))

;; Following https://github.com/minad/consult use-package example
(use-package consult
  ;; lazy loading
  :bind (;; C-c bindings in `mode-specific-map'
         ("C-c M-x" . consult-mode-command)
         ("C-c C h" . consult-history)
         ("C-c C k" . consult-kmacro)
         ("C-c C m" . consult-man)
         ("C-c C i" . consult-info)
	 ("C-s" . consult-line) ;; !! Additional binding (originally suggested under "M-s l" on https://github.com/minad/consult) overriding original emacs `isearch-forward'.
	 ("C-S-s" . elementaryx-consult-line-forward) ;; !! Additional binding for search with symbol at point
         ([remap Info-search] . consult-info)
         ;; C-x bindings in `ctl-x-map'
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x t b" . consult-buffer-other-tab)    ;; orig. switch-to-buffer-other-tab
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ;; M-g bindings in `goto-map'
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ;; ("M-g M-g" . consult-goto-line)        ;; orig. goto-line ;; we reserve it for avy
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g M" . consult-global-mark)           ;; Originally suggested under "M-g k" by minad
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings in `search-map'
         ("M-s f" . consult-find)                  ;; Originally suggested under "M-s d" by minad; Alternative: consult-fd
         ("M-s l" . consult-locate)                ;; Originally suggested under "M-s c" by minad
         ("M-s C-g" . consult-grep)    ;; Originally suggested under "M-s g" by minad
         ("M-s M-g" . consult-git-grep)  ;; Originally suggested under "M-s G" by minad
         ("M-s r" . consult-ripgrep)
         ("M-s g" . consult-ripgrep)   ;; Additional binding (not suggested by minad)
         ("M-s C-G" . elementaryx-consult-grep)    ;; Additional binding (not suggested by minad)
         ("M-s M-G" . elementaryx-consult-git-grep)  ;; Additional binding (not suggested by minad)
         ("M-s R" . elementaryx-consult-ripgrep)
         ("M-s G" . elementaryx-consult-ripgrep)   ;; Additional binding (not suggested by minad)
         ("M-s C-s" . isearch-forward)             ;; Additional binding (for original "C-s" emacs forward search)
	 ("M-s C-r" . isearch-backward)            ;; Additional binding (for original "C-r" emacs backward search)
         ("M-s L" . consult-line-multi)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Add consult "C-c C" leader key
  (which-key-add-key-based-replacements "C-c C" "consult")

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  (easy-menu-add-item nil
		      '("Edit") ; Insert withing the Edit menu
		      ["Paste (Consult)" consult-yank-pop :help "Consult previous copies, select and paste"]
		      "Paste from Kill Menu" ; Insert before this item/submenu
		      )

  (easy-menu-add-item nil
		      '("Edit") ; Insert withing the Edit menu
		      '("Search (Consult)"
			["Search"  consult-line :help "Search in the current buffer"]
			["Search in Project Files"  consult-ripgrep :help "Search in the files of the current project or directory (with `rg')"]
			["Search in Project Buffers"  consult-line-multi :help "Search in buffers of the current project"]
			["Search in All Buffers"  (consult-line-multi t) :help "Search in all buffers"]
			["Search with Git Grep"  consult-git-grep :fhelp "Search with `git grep' in the files of the current project"]
			)
		      "Search" ; Insert before this item/submenu
		      )

  (easy-menu-add-item nil
		      '("File") ; Insert withing the Edit menu
		      '("Find file (Consult)"
			["Find File in Project"  consult-find :help "Search for files with `find'"]
			["Locate File" consult-locate :help "Search with ‘locate’ for files, assuming an up-to-date database index (updatedb otherwise)."])
		      "Open File..." ; Insert before this item/submenu
		      )

  (easy-menu-add-item nil
		      '("Buffers")     ; Insert within the Buffersmenu
		      ["Consult and Select Named Buffer" consult-buffer :help "Select buffer"]
		      "Next Buffer"  ; Insert before this item/submenu
		      )

  (easy-menu-define
    elementaryx-go-map
    nil
    "Menu used to Go to somewhere. Essentially based on top of Consult (and Avy for quick navigation). Mimic (and help discovering) the M-g keyboard keymap."
    '("Go"
      ("Quick navigation (Avy)"
       ["Go to Line"  avy-goto-line :help "Jump to a line start on the screen"]
       ["Go to Char Timer"  avy-goto-char-timer :help "Read one or many consecutive chars (with time limit) and jump to the first one after possible decision tree"]
       )
      "----"
      ["Go to Imenu Entry (buffer)" consult-imenu :help "Select item from flattened ‘imenu’ using ‘completing-read’ with preview."]
      ["Go to Imenu Entry (project)" consult-imenu-multi :help "Select item from the imenus of all buffers from the same project."]
      "----"
      ["Go to Compile Error" consult-compile-error :help "Jump to a compilation error in the current buffer"]
      ["Go to Flymake Diagnostic" consult-flymake :help "Jump to Flymake diagnostic"]
      "----"
      ["Go to Line Number" consult-goto-line :help "Read line number and jump to the line with preview"]             "----"
      ["Go to Outline Heading" consult-outline :help "Jump to an outline heading obtained by matching against outline-regexp"]
      "----"
      ["Go to Mark" consult-mark :help "Jump to a marker in Markers list (defaults to buffer-local mark-ring). The mark ring is a data structure that keeps track of previous cursor positions, known as ‘marks’ within a buffer. It's essentially a stack where each entry represents a position in the buffer where you've set the mark."]
      ["Go to Global Mark" consult-global-mark :help "Jump to a marker in Markers list (defaults to global-mark-ring). The mark ring is a data structure that keeps track of previous cursor positions, known as ‘marks’ within a buffer. It's essentially a stack where each entry represents a position in the buffer where you've set the mark."]
      "----"
      ["Manual" (info "(consult)") :help "Manual of Consult"])
    )

  (define-key-after
    (lookup-key global-map [menu-bar])
    [elementaryx-go-map]
    (cons "Go" elementaryx-go-map)
    'edit)
  ;; keymap-set-after should be used instead of define-key-after since emacs 29.1
  ;; but I did not figure out how to adapt it

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config
  ;; We define rewrap consult-line, consult-grep, consult-ripgrep,
  ;; consult-git-grep to start with current symbol at point
  ;; See https://github.com/minad/consult/wiki#start-consult-line-search-with-symbol-at-point
  (defun elementaryx-consult-line-forward ()
    "Search for a matching line forward."
    (interactive)
    ;; (consult-line))
    (consult-line (thing-at-point 'symbol)))
  (defun elementaryx-consult-grep ()
    "Search for a matching line forward."
    (interactive)
    (consult-grep (thing-at-point 'symbol)))
  (defun elementaryx-consult-ripgrep ()
    "Search for a matching line forward."
    (interactive)
    (consult-ripgrep (thing-at-point 'symbol)))
  (defun elementaryx-consult-git-grep ()
    "Search for a matching line forward."
    (interactive)
    (consult-git-grep (thing-at-point 'symbol)))

  ;; Toggle preview during active completion session
  ;; https://github.com/minad/consult/wiki#toggle-preview-during-active-completion-session
  ;;
  ;; We enable/disable preview during an active completing-read
  ;; session. Bound to F5.
  (defvar-local elementaryx-consult-toggle-preview-orig nil)

  (defun elementaryx-consult-toggle-preview ()
    "Command to enable/disable preview."
    (interactive)
    (if elementaryx-consult-toggle-preview-orig
	(setq consult--preview-function elementaryx-consult-toggle-preview-orig
              elementaryx-consult-toggle-preview-orig nil)
      (setq elementaryx-consult-toggle-preview-orig consult--preview-function
            consult--preview-function #'ignore)))

  (define-key vertico-map (kbd "<f5>") #'elementaryx-consult-toggle-preview)
  ;; Live previews
  ;; https://github.com/minad/consult?tab=readme-ov-file#live-previews
  ;; Some Consult commands support live previews. For example when you
  ;; scroll through the items of consult-line, the buffer will scroll
  ;; to the corresponding position. Consult enables previews by
  ;; default. A safe recommendation is to leave automatic immediate
  ;; previews enabled in general and disable the automatic preview
  ;; only for commands where the preview may be expensive due to file
  ;; loading.
  (consult-customize
   ;; Set up a 0.2 delay for consult-theme preview
   consult-theme :preview-key '(:debounce 0.2 any)
   ;; Set up a 0.4 delay for other long loading preview commands
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
  ;;;; 2. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
  ;;;; 3. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  ;;;; 4. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
  ;;;; 5. No project support
  ;; (setq consult-project-function nil)
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;; Refine search workflow ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Type `C-s' to search forward and `C-r' to search backward. The
;; latter requires the vertico-reverse extension to be enabled,
;; following
;; https://github.com/minad/consult/wiki#isearch-like-backwardforward-consult-line

;; While `C-s' starts the forward search with an empty string, it is alternatively possible to use `C-S' (i.e; `C-S-s') forward search starts with the symbol at point while entering the completion mini-buffer
;; https://github.com/minad/consult/wiki#start-consult-line-search-with-symbol-at-point

;; Once in the mini-buffer, another `C-s' changes the search with next
;; element. As a consequence triggering a search with `C-s C-s' will
;; reach the previous search sequence. It will be about the same state
;; as vanilla emacs, except that we will also reach the states of
;; aborpted searches (e.g. that have been aborpted with `C-g').

;; Note that once in the consult completion buffer, you can hit `M-r',
;; `M-s' or `C-c C-h' to get the `consult-history'

;; --- We first refine the search forward workflow ---

(use-package consult
  :defer t
  :config
  ;; Customize search forward prompt
  (consult-customize consult-line
                     :prompt "Search forward: ")
  (consult-customize elementaryx-consult-line-forward
                     :prompt "Search forward: ")

  ;; Once in the minibuffer, we redefine `C-s' behaviour in 3 steps:

  ;; 1/3: Add category-specific minibuffer keybindings
  ;; https://github.com/minad/consult/wiki#add-category-specific-minibuffer-keybindings
  ;; The following function creates minibuffer keybindings that take effect
  ;; only for specific categories of items.

  (defun define-minibuffer-key (key &rest defs)
    "Define KEY conditionally in the minibuffer.
DEFS is a plist associating completion categories to commands."
    (define-key minibuffer-local-map key
		(list 'menu-item nil defs :filter
		      (lambda (d)
			(plist-get d (completion-metadata-get
				      (completion-metadata (minibuffer-contents)
							   minibuffer-completion-table
							   minibuffer-completion-predicate)
				      'category))))))

  ;; 2.1/3: Apply it for `C-s' keybinding.
  ;; The following binds C-s to previous-history-element
  ;; not only in consult-line, but also in consult-oultine,
  ;; consult-mark, etc; moreover, it binds C-s to
  ;; consult-find-for-minibuffer (defined below) whenever
  ;; the minibuffer is reading a file name.

  (define-minibuffer-key "\C-s"
			 'consult-location #'consult-history ;; Was: #'previous-history-element
			 'consult-grep     #'consult-history ;; Added
			 'file #'elementaryx-consult-find-for-minibuffer)

  ;; 2.2/3 We also apply for `C-r' keybinding

  (define-minibuffer-key "\C-r"
			 'consult-location #'consult-history ;; Was: #'previous-history-element
			 'consult-grep     #'consult-history ;; Added
			 'file #'elementaryx-consult-find-for-minibuffer)
  ;; Note that "M-r" (or "M-s" or "C-c h" will also enter the
  ;; consult-history).
  ;; Alternatively, it would be possible to re-establish standard
  ;; isearch-backward once in the mini-buffer:
  ; (define-key minibuffer-local-map "\C-r" #'isearch-backward)

  ;; 3/3: in the `file' case, we still need to define the
  ;; `elementaryx-consult-find-for-minibuffer' function
  ;; https://github.com/minad/consult/wiki#using-find-in-the-minibuffer

  ;; The following command, meant to be called in the minibuffer when
  ;; it is reading a file name, switches from the usual hierarchical
  ;; browsing of the file system to a consult-find session.

  ;; The code was patched below.
  ;; TODO: if the fix remains robust, report upstream
  (defun elementaryx-consult-find-for-minibuffer ()
    "Search file with find, enter the result in the minibuffer."
    (interactive)
    (let* ((enable-recursive-minibuffers t)
           (default-directory (file-name-directory (minibuffer-contents)))
           (file (consult--find
                  (replace-regexp-in-string
                   "\\s-*[:([].*"
                   (format " (via find in %s): " default-directory)
                   (minibuffer-prompt))
                  (consult--find-make-builder nil) ;; PATCH: the proposed code did not have nil argument
                  (file-name-nondirectory (minibuffer-contents)))))
      (delete-minibuffer-contents)
      (insert (expand-file-name file default-directory))
      (exit-minibuffer))))

;; --- We then define the search backward workflow

(use-package vertico-reverse
  :after (vertico consult)
  :bind
  ("C-r" . 'elementaryx-consult-line-backward)
  :config
  ;; Search backward
  ;; https://github.com/minad/consult/wiki#isearch-like-backwardforward-consult-line
  (defun elementaryx-consult-line-backward ()
    "Search for a matching line backward."
    (interactive)
    (advice-add 'consult--line-candidates :filter-return 'reverse)
    (vertico-reverse-mode +1)
    (unwind-protect (consult-line (thing-at-point 'symbol)) ;; Add thing at point consistently with search forward following https://github.com/minad/consult/wiki#start-consult-line-search-with-symbol-at-point
      (vertico-reverse-mode -1)
      (advice-remove 'consult--line-candidates 'reverse)))
  (consult-customize elementaryx-consult-line-backward
                     :prompt "Search backward: "))

;;;;;;;;;;;;;;;;;
;; XDG support ;;
;;;;;;;;;;;;;;;;;

(use-package xdg) ;; Seems that consult-xdg-recent-files lacks to require xdg
;; https://github.com/hrehfeld/consult-xdg-recent-files/blob/main/consult-xdg-recent-files.el

(use-package consult-xdg-recent-files
  :after (consult xdg)
  :if (file-exists-p (expand-file-name "recently-used.xbel" (xdg-data-home)))
  :config
  ;; We add system recent files (~/.local/share/recently-used.xbel) to
  ;; the tail ('t') of the consult-buffer-sources
  (add-to-list 'consult-buffer-sources consult-xdg-recent-files--source-system-file t))

;;;;;;;;;;;;
;; Embark ;;
;;;;;;;;;;;;

(use-package embark
  :demand t
  :after avy
  :bind (("C-." . embark-act)  ;; pick some comfortable binding
	 ("C-c ." . embark-act)  ;; Add alternative binding (some terminal emulators may not handle properly "C-." -- see https://catern.com/posts/terminal_quirks.html)
	 ("C-c M-." . embark-act)  ;; Yet another alternative binding (some modes may use "C-c ." such as c++-ts-mode)
	 ("M-." . embark-dwim) ;; good suggested options: C-; or M-.
	 ("C-c E" . embark-export)
	 ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Add the option to run embark when using avy
  (defun elementaryx/avy-action-embark (pt)
    (unwind-protect
        (save-excursion
          (goto-char pt)
          (embark-act))
      (select-window
       (cdr (ring-ref avy-ring 0))))
    t)

  ;; After invoking avy-goto-char-timer, hit "." to run embark at the next
  ;; candidate you select
  (setf (alist-get ?. avy-dispatch-alist) 'elementaryx/avy-action-embark))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; embark and consult allow for efficient search/edit worklows;
;; https://github.com/minad/consult?tab=readme-ov-file#embark-integration
;;
;; These three workflows are symmetric:
;; - consult-line -> embark-export to occur-mode buffer -> occur-edit-mode for editing of matches in buffer.
;; - consult-grep -> embark-export to grep-mode buffer -> wgrep for editing of all matches.
;; - consult-find -> embark-export to dired-mode buffer -> wdired-change-to-wdired-mode for editing.

;; In the following we try to them up with a consistent user interface workflow to reach an edit

;;;;;;;;;;;;;;;;;;;;;;;;;
;; Edit (after Search) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

;; We propose the following key sequence:
;; 1. `C-. E'               : embark-export
;; 2. `E'                   : switch to edit mode
;; 3. `C-c C-c'             : switch back to non-edit mode

;; After step 1 `C-. E' (embark-export), we may be in `occur-mode',
;; `grep-mode' or `dired-mode'.

;; In all cases, for step 2, we bind `E' to switch to the
;; corresponding editable mode:
;; - In `occur-mode', `e' is already the default keybinding to switch to `occur-edit-mode', we add up `E'
;; - In `grep-mode', we use `E' rather than default `C-c C-p' keybinding to switch to `wgrep-mode'.
;; - In `dired-mode', there is not default keybinding to switch to `wdired-mode' and we set it as `E'.

;; To help discovering the switch to step 2, we add up a message
;; stating which key to press (`E') to enter the mode. The message is
;; delayed of 0.1 second so that other potential messages (Quit, ...)
;; do not immediately hide it.

;; For step 3, `C-c C-c' is a default binding to switch back to
;; non-edit mode for all three cases. Note that `occur-edit-mode'
;; changes apply in real time to the corresponding buffers. On the
;; contrary, `wgrep-mode' and `wdired-mode' changes apply only after
;; switch back to non-edit mode (and in those modes it is possible to
;; revert changes with `C-c C-k' instead of `C-c C-c').

(use-package replace
  :bind (:map occur-mode-map
              ("E" . occur-edit-mode))
  :hook (occur-mode . (lambda ()
			(when (not (derived-mode-p 'occur-edit-mode))
                          (run-at-time 0.1 nil
                                       (lambda ()
					 (message (substitute-command-keys
                                                   "Press \\[occur-edit-mode] to switch to Edit mode (with occur-edit-mode)."))))))))

(use-package wgrep
  :commands wgrep-change-to-wgrep-mode
  :config
  (setq wgrep-enable-key "E") ;; default is otherwise C-c C-p
  (setq wgrep-auto-save-buffer t)
  :hook (grep-mode . (lambda ()
		       (when (not (derived-mode-p 'wgrep-mode))
                         (run-at-time 0.1 nil
                                      (lambda ()
					(message (substitute-command-keys
                                                  "Press \\[wgrep-change-to-wgrep-mode] to switch to Edit mode (with wgrep-mode)."))))))))

(use-package dired
  :bind (:map dired-mode-map
              ("E" . wdired-change-to-wdired-mode))
  :hook (dired-mode . (lambda ()
			(when (not (derived-mode-p 'wdired-mode))
                          (run-at-time 0.1 nil
                                       (lambda ()
					 (message (substitute-command-keys
                                                   "Press \\[wdired-change-to-wdired-mode] to Edit (with wdired-mode)."))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Minibuffer and completion
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Vertico: better vertical completion for minibuffer commands
;; From: https://github.com/minad/vertico
(use-package vertico
  :init
  (vertico-mode)

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates (default is 10 otherwise)
  (setq vertico-count 15)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t)
  )

(use-package vertico-mouse
  :init
  (vertico-mouse-mode))

;; Persist history over Emacs restarts. Vertico sorts by history position.
;; From: https://github.com/minad/vertico
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations...
;; From: https://github.com/minad/vertico
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

(use-package vertico-directory
  :after vertico
  :bind (:map vertico-map
              ("M-DEL" . vertico-directory-delete-word)))

;; Marginalia: annotations for minibuffer
(use-package marginalia
  :config
  (marginalia-mode))

;; Popup completion-at-point
(use-package corfu
  :init
  (global-corfu-mode)
  :bind
  (:map corfu-map
        ("SPC" . corfu-insert-separator)
        ("C-n" . corfu-next)
        ("C-p" . corfu-previous)))

;; Part of corfu
(use-package corfu-popupinfo
  :after corfu
  :hook (corfu-mode . corfu-popupinfo-mode)
  :custom
  (corfu-popupinfo-delay '(0.25 . 0.1))
  (corfu-popupinfo-hide nil)
  :config
  (corfu-popupinfo-mode))

;; Make corfu popup come up in terminal overlay
(use-package corfu-terminal
  :if (not (display-graphic-p))
  :config
  (corfu-terminal-mode))

;; Fancy completion-at-point functions; there's too much in the cape package to
;; configure here; dive in when you're comfortable!
(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file))

;; Pretty icons for corfu
(use-package kind-icon
  :if (display-graphic-p)
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package eshell
  :init
  (defun bedrock/setup-eshell ()
    ;; Something funny is going on with how Eshell sets up its keymaps; this is
    ;; a work-around to make C-r bound in the keymap
    (keymap-set eshell-mode-map "C-r" 'consult-history))
  :hook ((eshell-mode . bedrock/setup-eshell)))

;; Orderless: powerful completion style
(use-package orderless
  :config
  (setq completion-styles '(orderless basic)
	completion-category-overrides '((file (styles basic partial-completion)))))

;; Orderless refinement.
;; Orderless allows one by default to search for a component literally by prefixing it with `='.
;; We here add a "C-=" keybinding to switch to literal search on all components
;; Example derived from https://github.com/oantolin/orderless?tab=readme-ov-file#interactively-changing-the-configuration but with minibuffer-local-map rather than the more restricted minibuffer-local-completion-map
(use-package orderless
  :demand t
  :bind (:map minibuffer-local-map
              ("C-=" . elementaryx-orderless-match-components-literally))
  :config
  (defun elementaryx-orderless-match-components-literally ()
    "Components match literally for the rest of the session."
    (interactive)
    (setq-local orderless-matching-styles '(orderless-literal)
                orderless-style-dispatchers nil)
    (message "Toggled to literal search")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Escaping a sequence (C-g)
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Consult heavily relies on minibuffer, possibly in a nested fashion.
;; We need to set up a neat quit sequence closing (windows associated
;; to) nested mini-buffers. Here is a simple and effective setup to
;; do so. Credits to Protesilaos Stavrou https://protesilaos.com/emacs/dotemacs
;; See its prot-simple-keyboard-quit-dwim function.

(defun elementaryx-keyboard-quit ()
  "Do-What-I-Mean behaviour for a general `keyboard-quit'.

The generic `keyboard-quit' does not do the expected thing when
the minibuffer is open.  Whereas we want it to close the
minibuffer, even without explicitly focusing it.

The DWIM behaviour of this command is as follows:

- When the region is active, disable it.
- When a minibuffer is open, but not focused, close the minibuffer.
- When the Completions buffer is selected, close it.
- In every other case use the regular `keyboard-quit'."
  (interactive)
  (cond
   ;; Case 0: When multiple-cursor is active, quit it with mc/keyboard-quit
   ;; Nothing to be done: already handled through mc/keymap by default.

   ;; Case 1: If a region is active, quit it with `keyboard-quit`
   ((region-active-p)
    (keyboard-quit))

   ;; Case 2: If we're in `completion-list-mode` (e.g., in a completion buffer), close the completion window
   ((derived-mode-p 'completion-list-mode)
    (delete-completion-window))

   ;; Case 3: If the minibuffer is active (depth > 0), abort it using `abort-recursive-edit`
   ((> (minibuffer-depth) 0)
    (abort-recursive-edit))

   ;; Default case: Perform the normal `keyboard-quit` behavior (quit other active processes)
   (t
    (keyboard-quit))))

;; We rebind C-g (originally bound to keyboard-quit) to elementaryx-keyboard-quit
(global-set-key (kbd "C-g") 'elementaryx-keyboard-quit)

;; In the particular case of the Elementaryx ESCode setup, we also
;; bind to elementaryx-keyboard-quit to ESC ESC, close to the typical
;; escape key ESC of VSCode. We thus loose the access to
;; keyboard-escape-quit (ESC ESC ESC) which is re-bounded to C-ESC.
(use-package emacs
  :defer t
  :after elementaryx-escode
  :config
  (global-set-key (kbd "ESC ESC") 'elementaryx-keyboard-quit) ;; Typical VSCode escape sequence
  (global-set-key (kbd "C-<escape>") 'keyboard-escape-quit))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Undo / Redo
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Windows-like / VS Code-like undo / redo
;; Note that `suspend-frame' `C-z' is also natively available under `C-x C-z' in emacs.
(use-package undo-fu
  :bind (("C-z"   . undo-fu-only-undo)
         ("C-S-z" . undo-fu-only-redo))
  :custom
  (undo-fu-allow-undo-in-region t)) ;; with a selection, limit undo/redo to a region

(use-package undo-fu
  :defer t
  :after evil
  :custom
  (evil-undo-system 'undo-fu))

(use-package undo-fu
  :defer t
  :after elementaryx-escode
  :config
  (global-set-key (kbd "C-y") 'undo-fu-only-redo)) ;; VSCode redo binding on Linux

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Base emacs-elementaryx prefix-maps for key-binding
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package emacs
  ;; See https://github.com/jwiegley/use-package/blob/master/bind-key.el
  ;; for defining prefix map (useful for which-key)
  :bind
  (:prefix-map insert-elementaryx-map
	       :prefix-docstring "insert"
	       :prefix "C-c i")
  (:prefix-map toggle-elementaryx-map
	       :prefix-docstring "toggle"
	       :prefix "C-c t") ;; TODO: WARNING Be cautious
  :init
  (which-key-add-key-based-replacements "C-c i" "insert")
  (which-key-add-key-based-replacements "C-c t" "toggle"))

;;;;;;;;;;;;;;;;
;;; Terminal ;;;
;;;;;;;;;;;;;;;;

;; Prevent vterm to show trailing whitespaces
(use-package vterm
  :hook (vterm-mode . (lambda ()
                        (setq-local show-trailing-whitespace nil))))

(use-package vterm-toggle
  :bind
  (:map toggle-elementaryx-map
	("v" . vterm-toggle)
	("V" . vterm-toggle-cd)))

(use-package multi-vterm
  :init
  (defun elementaryx/named-vterm (term-name)
    "Generate a terminal with buffer name TERM-NAME."
    (interactive "sTerminal purpose: ")
    (vterm (concat "*vterm-" term-name "*")))
  :bind
  (("C-x p v" . multi-vterm-project)) ;; override project-shell
  (:map toggle-elementaryx-map
	("w" . elementaryx/named-vterm)))


;;;;;;;;;;;;;
;; guix.el ;;
;;;;;;;;;;;;;

;; https://github.com/alezost/guix.el
(use-package guix
  :hook
  (scheme-mode . guix-devel-mode)           ;; highlights various Guix keywords.
  (shell-mode . guix-build-log-minor-mode)  ;; highlights build log in the current buffer.
  (after-init . global-guix-prettify-mode)) ;; hash parts of the Guix store file names are displayed as ‘guix-prettify-char’ characters

;;;;;;;;;
;; pdf ;;
;;;;;;;;;

(use-package pdf-tools
  :mode  ("\\.pdf\\'" . pdf-view-mode)
  :config
  (setq-default pdf-view-display-size 'fit-page)
  (pdf-tools-install :no-query)
  ;; (require 'pdf-occur)
  :custom
  (pdf-annot-activate-created-annotations t))

;;; TO REFILE

(setq org-file-apps
      (quote
       ((auto-mode . emacs)
        ("\\.mm\\'" . default)
        ("\\.x?html?\\'" . "icecat %s")
        ("\\.pdf\\'" . emacs))))

;; TODO: refile
(setq vc-follow-symlinks t)

(provide 'elementaryx-base)
